let AWS = require('aws-sdk');
const ses = new AWS.SES();
const ddb = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {
    ddb.put({
        TableName: 'ContactDemo',
        Item: { 'id': event.companyId, 'sortKey': event.formHash, 'name': event.name, 'email': event.email }
    }).promise()
        .then((data) => {
            ses.sendEmail({
                Destination: {
                    ToAddresses: ['tomas@appsystem.se'],
                    CcAddresses: [],
                    BccAddresses: []
                },
                Message: {
                    Body: {
                        Html: {
                            Data: 'Inskickat!'
                        }
                    },
                    Subject: {
                        Data: 'Inskickat kontaktformulär'
                    }
                },
                Source: 'tomas@digiling.com',
            }, function (err, data) {
                if (err) console.log(err, err.stack); // an error occurred
                else console.log(data);           // successful response
            });
            callback(null, { "message": "Inskickat kontaktformulär" });
        })
        .catch((err) => {
            callback(null, { "message": "An error occured" });
        });

}